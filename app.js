let express = require("express");
let app = express();
let server = require("http").createServer(app);
let io = require("socket.io")(server);
const accountSid = "ACddf97b143307ff6edadb6c389c371255";
const authToken = "abbbfcda953df5cbd9d4883926bc8ca5";
const client = require("twilio")(accountSid, authToken);
const users = {};
let socketIdToNames = {};
server.listen(8080, function() {
  console.log("server is running at 8080");
});

//To get the STUN/TURN servers from the twillio
// client.tokens
//   .create()
//   .then(token => {
//     console.log("--------------------->>>.", token);
//     console.log(token.username);
//   })
//   .done();
function socketIdsInRoom(roomId) {
  let socketIds = io.nsps["/"].adapter.rooms[roomId];
  if (socketIds) {
    let collection = [];
    for (let key in socketIds) {
      collection.push(key);
    }
    return collection;
  } else {
    return [];
  }
}
io.on("connection", function(socket) {
  console.log("connected", socket.id);

  socket.on("init", data => {
    users[data.user] = socket;
  });

  socket.on("disconnect", function() {
    console.log("in the disconnnect", socket.id);
    delete users[socketIdToNames[socket.id]];
    delete socketIdToNames[socket.id];
    let room = socket.room;
    if (socket.room) {
      io.to(room).emit("disconnect", socket.id);
      socket.leave(room);
    }
  });
  socket.on("endCall", function({ fromUser, toUser }) {
    console.log("end call,socket.id");
    console.log("from and tooooooooooooo", fromUser, toUser);
    const roomId = [fromUser, toUser].sort().join("");
    socket.leave(roomId);
    users[toUser.toString()].leave(roomId);
    users[toUser.toString()].emit("endCall", { fromUser });
  });
  /**
   * Callback: list of {socketId, name: name of user}
   */
  socket.on("calling", function({ fromUser, toUser }) {
    console.log("to user0----", toUser);
    users[toUser.toString()].emit("calling", { fromUser });
  });

  socket.on("reject", function({ fromUser, toUser }) {
    console.log("to user0----", toUser);
    const anotherUserSocket = users[toUser.toString()];
    anotherUserSocket.emit("reject", { fromUser });
    const room = anotherUserSocket.room;
    if (room) anotherUserSocket.leave(room);
  });

  socket.on("join", function(joinData, callback) {
    //Join room
    let roomId = joinData.roomId;
    let name = joinData.name;
    socket.join(roomId);
    socket.room = roomId;
    socketIdToNames[socket.id] = name;
    let socketIds = socketIdsInRoom(roomId);
    let friends = socketIds
      .map(socketId => {
        return {
          socketId: socketId,
          name: socketIdToNames[socketId]
        };
      })
      .filter(friend => friend.socketId != socket.id);
    callback(friends);
    //broad
    friends.forEach(friend => {
      io.sockets.connected[friend.socketId].emit("join", {
        socketId: socket.id,
        name
      });
    });
  });

  socket.on("exchange", function(data) {
    data.from = socket.id.toString();
    console.log("lsjfdlsfdkfjslfjsodf", data.to);
    let to = io.sockets.connected[data.to.toString()];
    to.emit("exchange", data);
  });

  socket.on("count", function(roomId, callback) {
    let socketIds = socketIdsInRoom(roomId);
    callback(socketIds.length);
  });
});
